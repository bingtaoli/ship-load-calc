create database if not exists ship character set utf8;
use ship;

# equipment table
create table if not exists equip (
  id int NOT NULL AUTO_INCREMENT,
  name varchar(20) UNIQUE NOT NULL,
  number int NOT NULL,
  belong VARCHAR(20) NOT NULL, #所属大类
#电机额定数据
  p1 FLOAT, #功率
  n FLOAT,  #效率, P4 = P1/N; P5 = P4 * m;
#航行状态
  k2_sail FLOAT DEFAULT 0, #机械负荷系数
  k3_sail FLOAT DEFAULT 0, #电动机负荷系数
  k0_sail FLOAT DEFAULT 0, #同时使用系数, 所需有效功率为 K0*K3*P5
  sail_load_type int DEFAULT 0, #负荷类别
#进出港状态
  k2_inout FLOAT DEFAULT 0, #机械负荷系数
  k3_inout FLOAT DEFAULT 0, #电动机负荷系数
  k0_inout FLOAT DEFAULT 0, #同时使用系数, 所需有效功率为 K0*K3*P5
  inout_load_type int DEFAULT 0, #负荷类别
  #作业状态
  k2_work FLOAT DEFAULT 0, #机械负荷系数
  k3_work FLOAT DEFAULT 0, #电动机负荷系数
  k0_work FLOAT DEFAULT 0, #同时使用系数, 所需有效功率为 K0*K3*P5
  work_load_type int DEFAULT 0, #负荷类别
  #停泊状态
  k2_anchor FLOAT DEFAULT 0, #机械负荷系数
  k3_anchor FLOAT DEFAULT 0, #电动机负荷系数
  k0_anchor FLOAT DEFAULT 0, #同时使用系数, 所需有效功率为 K0*K3*P5
  anchor_load_type int DEFAULT 0, #负荷类别
  PRIMARY KEY (id)
) default charset=utf8;