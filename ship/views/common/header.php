<?php
/**
 * Created by PhpStorm.
 * User: libingtao
 * Date: 15/12/9
 * Time: 下午3:39
 */
?>
<head>
    <meta charset="UTF-8">
    <title>运输船电力负荷计算系统</title>
    <?php $this->load->view('common/bootstrap');?>
    <link rel="stylesheet" type="text/css" href="<?php if(DIR_IN_ROOT) echo '/' . DIR_IN_ROOT ?>/public/styles/ship.css" />
</head>
<body>
<header>
    <div class="container">
        <div class="fl dian-logo">运输船电力负荷计算系统</div>
        <div class="clear"></div>
    </div>
</header>
