<?php
/**
 * Created by PhpStorm.
 * User: bingtao
 * Date: 2015/12/6
 * Time: 13:35
 */
?>

<!doctype html>
<html>
<?php $this->load->view('common/header') ?>
<style>
    .col-md-2, .col-md-9 {
        padding: 0;
    }
    .panel-heading {
        color: #434A54;
        background-color: #E6E9ED;
        border-color: #E6E9ED;
    }
</style>
<div class="container">
    <div class="alert alert-warning alert-dismissible" role="alert" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="message"></span>
    </div>
    <div class="alert alert-info alert-dismissible" role="alert" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="message"></span>
    </div>
    <div class="col-md-2 offset-left">
        <ul class="list-group">
            <li class="list-group-item">电动液压舵机</li>
            <li class="list-group-item">电动绞盘</li>
            <li class="list-group-item">电动起锚机</li>
            <li class="list-group-item">起重机</li>
        </ul>
    </div>
    <div class="col-md-9 offset-right">
        <div class="panel-heading">
            <h3 class="panel-title">电动液压舵机</h3>
        </div>
        <div class="form-group">
           <span>额定功率: 5KW</span>
        </div>
        <div class="form-group">
           <span>效率: 89%</span>
        </div>
    </div>
</div>

<div id="add-media-modal" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">增加媒体</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>媒体名称</label>
                    <input name="media_name" type="text" class="form-control">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="add-media-confirm-btn" type="button" class="btn btn-primary">确定增加</button>
            </div>
        </div>
    </div>
</div>