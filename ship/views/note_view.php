<?php
/**
 * Created by PhpStorm.
 * User: libingtao
 * Date: 15/12/23
 * Time: 下午3:16
 */
?>
<!DOCTYPE html>
<html lang="en" class="app">
<head>
    <meta charset="utf-8" />
    <title>运输船电力负荷计算系统</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" type="text/css" href="<?php if(DIR_IN_ROOT) echo '/' . DIR_IN_ROOT ?>/public/css/app.v2.css" />
    <script src="<?php if(DIR_IN_ROOT) echo '/' . DIR_IN_ROOT ?>/public/third_part/jquery-2.1.4.min.js"></script>
    <style>
        .hbox > .none {
            display: none;
        }
        .table>tbody>tr>td {
            border-bottom: 1px solid #dddddd;;
        }
        ul.nav li {
            cursor: pointer;
        }
        .ship-features {
            display: inline-block;
            width: 90px;
        }
    </style>
</head>
<body>
<section class="vbox">
    <header class="bg-dark dk header navbar navbar-fixed-top-xs">
        <div class="navbar-header">
            <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav">
                <i class="fa fa-bars"></i>
            </a>
            <!--点击全屏，简直sigoyi-->
            <a href="#" class="navbar-brand" data-toggle="fullscreen"><img src="<?php if(DIR_IN_ROOT) echo '/' . DIR_IN_ROOT ?>/public/images/logo.png" class="m-r-sm">运输船电力负荷计算系统</a>
        </div>
    </header>
    <section class="hbox stretch"> <!-- .aside -->
        <aside class="bg-dark lter aside-md hidden-print" id="nav">
            <section class="vbox">
                <section class="w-f scrollable">
                    <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="5px" data-color="#333333"> <!-- nav -->
                        <nav class="nav-primary hidden-xs">
                            <ul class="nav">
                                <li class="active">
                                    <a href="#" class="active" id="index"><span>计算</span></a>
                                </li>
                                <li>
                                    <a href="#" class="active" id="add-ship"><span>增加设备</span></a>
                                </li>
                                <?php $i=0; foreach($data as $type => $items){ $i = $i + 1; ?>
                                <li class="belong-item">
                                    <a>
                                        <span class="pull-right"><i class="fa fa-angle-down text"></i><i class="fa fa-angle-up text-active"></i></span>
                                        <span><?php echo $type; ?></span>
                                    </a>
                                    <ul class="nav lt">
                                        <?php $j=0; foreach($items as $item){ $j = $j + 1; ?>
                                        <li class="ship-item" name="<?php echo $i . "_" . $j; ?>" ><a><span><?php echo $item['name'] ?></span></a></li>
                                        <?php } ?>
                                    </ul>
                                </li>
                                <?php } ?>
                            </ul>
                        </nav>
                        <!-- / nav -->
                    </div>
                </section>
            </section>
        </aside>
        <!-- /.aside -->
        <section id="index-target">
            <section class="vbox">
                <section class="scrollable padder">
                    <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                        <li><i class="fa fa-home"></i>Home</li>
                        <li class="active">calculator</li>
                    </ul>
                    <div class="m-b-md">
                        <h3 class="m-b-none">welcome</h3>
                        <small>欢迎使用运输船电力负荷计算系统!</small>
                    </div>
                    <div>
                        <button id="calculate" class="btn btn-s-md btn-primary btn-rounded">计算负荷</button>
                    </div>
                    <hr>
                    <div style="height: 500px;">
                        <div id="calc-result" class="table-responsive">
                            <table class="sail-state table table-striped b-t b-light text-sm table-bordered" style="width: 70%;">
                                <h3 class="m-b-none" style="margin-bottom: 10px;">计算结果</h3>
                                <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>航行状态</th>
                                    <th>进出港状态</th>
                                    <th>作业状态</th>
                                    <th>停泊状态</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>I类负荷总功率/KW</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>II类负荷总功率/KW</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>III类负荷总功率/KW</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>I类负荷考虑总同时使用系数0.9时总功率/KW</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>II类负荷考虑总同时使用系数0.6时总功率/KW</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>考虑总同时使用系数后I,II总功率/KW</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>考虑网络损失5%后所需总功率/KW</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>运行发电机/(台数xKW)</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>备用发电机/(台数xKW)</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>发电机负荷率/%</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </section>
        </section>
        <section id="add-ship-target" class="none">
            <section class="vbox">
                <section class="scrollable padder">
                    <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                        <li><i class="fa fa-home"></i>Home</li>
                        <li class="active">增加设备</li>
                    </ul>
                    <div class="m-b-md">
                        <h3 class="m-b-none">增加设备</h3>
                    </div>
                    <hr>
                    <div style="height: 750px;">
                        <div class="col-sm-6">
                            <label>设备名称</label>
                            <input name="equip-name" type="text" class="form-control">
                        </div>
                        <div class="col-sm-6">
                            <label>功率</label>
                            <input name="p1" type="text" class="form-control">
                        </div>
                        <div class="col-sm-6">
                            <label>效率</label>
                            <input name="n" type="text" class="form-control">
                        </div>
                        <div class="col-sm-6">
                            <label>航行状态K2</label>
                            <input name="k2_sail" type="text" class="form-control">
                        </div>
                        <div class="col-sm-6">
                            <label>航行状态K3</label>
                            <input name="k3_sail" type="text" class="form-control">
                        </div>
                        <div class="col-sm-6">
                            <label>航行状态K0</label>
                            <input name="k0_sail" type="text" class="form-control">
                        </div>
                        <div class="col-sm-6">
                            <label>航行负荷类别</label>
                            <input name="sail_load_type" type="text" class="form-control">
                        </div>
                        <div class="col-sm-6">
                            <label>进出港状态K2</label>
                            <input name="k2_inout" type="text" class="form-control">
                        </div>
                        <div class="col-sm-6">
                            <label>进出港状态K3</label>
                            <input name="k3_inout" type="text" class="form-control">
                        </div>
                        <div class="col-sm-6">
                            <label>进出港状态K0</label>
                            <input name="k0_inout" type="text" class="form-control">
                        </div>
                        <div class="col-sm-6">
                            <label>进出港负荷类别</label>
                            <input name="inout_load_type" type="text" class="form-control">
                        </div>
                        <div class="col-sm-6">
                            <label>作业状态K2</label>
                            <input name="k2_work" type="text" class="form-control">
                        </div>
                        <div class="col-sm-6">
                            <label>作业状态K3</label>
                            <input name="k3_work" type="text" class="form-control">
                        </div>
                        <div class="col-sm-6">
                            <label>作业状态K0</label>
                            <input name="k0_work" type="text" class="form-control">
                        </div>
                        <div class="col-sm-6">
                            <label>作业负荷状态类别</label>
                            <input name="work_load_type" type="text" class="form-control">
                        </div>
                        <div class="col-sm-6">
                            <label>停泊状态K2</label>
                            <input name="k2_anchor" type="text" class="form-control">
                        </div>
                        <div class="col-sm-6">
                            <label>停泊状态K3</label>
                            <input name="k3_anchor" type="text" class="form-control">
                        </div>
                        <div class="col-sm-6">
                            <label>停泊状态K0</label>
                            <input name="k0_anchor" type="text" class="form-control">
                        </div>
                        <div class="col-sm-6">
                            <label>停泊负荷状态类别</label>
                            <input name="anchor_load_type" type="text" class="form-control">
                        </div>
                        <div class="col-sm-6">
                            <label>所属大类</label>
                            <select name="belong" class="form-control m-b">
                                <option value="1">舾装设备</option>
                                <option value="2">轮机设备</option>
                                <option value="3">空调冷藏设备</option>
                                <option value="4">弱电及其他</option>
                            </select>
                        </div>
                        <div style="clear: both;"></div>
                        <div class="col-sm-6" style="margin-top: 12px;">
                            <button id="add-ship-cfm" type="button" class="btn btn-sm btn-default">确定增加</button>
                        </div>
                    </div>
                </section>
            </section>
        </section>
        <?php $i= 0; foreach($data as $type => $items){ $i = $i + 1; ?>
            <?php $j=0; foreach($items as $item){ $j = $j+1; ?>
                <section id="<?php echo $i . "_" . $j ?>" class="none ship-section">
                    <input name="id" type="hidden" value="<?php echo $item['id']; ?>">
                    <section class="vbox">
                        <section class="scrollable padder">
                            <div style="height: 770px;">
                                <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                                    <li><a href="#"><i class="fa fa-home"></i><?php echo $type ?></a></li>
                                    <li class="active"><?php echo $item['name'] ?></li>
                                </ul>
                                <p><span class="ship-features">功率(KW):</span><input type="text" name="p1" value="<?php if(isset($item['p1'])) echo $item['p1']; ?>"></p>
                                <p><span class="ship-features">效率(%):</span><input type="text" name="n" value="<?php if(isset($item['n'])) echo $item['n']; ?>"></p>
                                <p>
                                    <span class="ship-features">数量:</span>
                                    <input type="text" name="number" value="<?php if(isset($item['number']) && $item['number'] != 0) echo $item['number']; else echo "1"; ?>">
                                </p>
                                <div class="table-responsive">
                                    <table class="sail-state table table-striped b-t b-light text-sm">
                                        <h3 class="m-b-none">航行状态</h3>
                                        <thead>
                                        <tr>
                                            <th>k2</th>
                                            <th>k3</th>
                                            <th>k0</th>
                                            <th>负荷类别</th>
                                            <th width="30"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td name="k2_sail"><?php if(isset($item['k2_sail'])) echo $item['k2_sail']; else echo "-"; ?></td>
                                            <td name="k3_sail"><?php if(isset($item['k3_sail'])) echo $item['k3_sail']; else echo "-"; ?></td>
                                            <td name="k0_sail"><?php if(isset($item['k0_sail'])) echo $item['k0_sail']; else echo "-"; ?></td>
                                            <td name="sail_load_type"><?php if(isset($item['sail_load_type'])) echo $item['sail_load_type']; else echo "-"; ?></td>
                                            <td>
                                                <a href="#" class="active">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <h3 class="m-b-none">进出港状态</h3>
                                    <table class="inout-state table table-striped b-t b-light text-sm">
                                        <thead>
                                        <tr>
                                            <th>k2</th>
                                            <th>k3</th>
                                            <th>k0</th>
                                            <th>负荷类别</th>
                                            <th width="30"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td name="k2_inout"><?php if(isset($item['k2_inout'])) echo $item['k2_inout']; else echo "-"; ?></td>
                                            <td name="k3_inout"><?php if(isset($item['k3_inout'])) echo $item['k3_inout']; else echo "-"; ?></td>
                                            <td name="k0_inout"><?php if(isset($item['k0_inout'])) echo $item['k0_inout']; else echo "-"; ?></td>
                                            <td name="inout_load_type"><?php if(isset($item['inout_load_type'])) echo $item['inout_load_type']; else echo "-"; ?></td>
                                            <td>
                                                <a href="#" class="active" data-toggle="class">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <h3 class="m-b-none">作业状态</h3>
                                    <table class="work-state table table-striped b-t b-light text-sm">
                                        <thead>
                                        <tr>
                                            <th>k2</th>
                                            <th>k3</th>
                                            <th>k0</th>
                                            <th>负荷类别</th>
                                            <th width="30"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td name="k2_work"><?php if(isset($item['k2_work'])) echo $item['k2_work']; else echo "-"; ?></td>
                                            <td name="k3_work"><?php if(isset($item['k3_work'])) echo $item['k3_work']; else echo "-"; ?></td>
                                            <td name="k0_work"><?php if(isset($item['k0_work'])) echo $item['k0_work']; else echo "-"; ?></td>
                                            <td name="work_load_type"><?php if(isset($item['work_load_type'])) echo $item['work_load_type']; else echo "-"; ?></td>
                                            <td>
                                                <a href="#" class="active" data-toggle="class">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <h3 class="m-b-none">停泊状态</h3>
                                    <table class="anchor-state table table-striped b-t b-light text-sm">
                                        <thead>
                                        <tr>
                                            <th>k2</th>
                                            <th>k3</th>
                                            <th>k0</th>
                                            <th>负荷类别</th>
                                            <th width="30"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td name="k2_anchor"><?php if(isset($item['k2_anchor'])) echo $item['k2_anchor']; else echo "-"; ?></td>
                                            <td name="k3_anchor"><?php if(isset($item['k3_anchor'])) echo $item['k3_anchor']; else echo "-"; ?></td>
                                            <td name="k0_anchor"><?php if(isset($item['k0_anchor'])) echo $item['k0_anchor']; else echo "-"; ?></td>
                                            <td name="anchor_load_type"><?php if(isset($item['anchor_load_type'])) echo $item['anchor_load_type']; else echo "-"; ?></td>
                                            <td>
                                                <a href="#" class="active">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div>
                                    <button class="btn btn-default update-equip">更新该设备</button>
                                    <button class="btn btn-default del-equip">删除该设备</button>
                                </div>
                            </div>
                        </section>
                    </section>
                </section>
            <?php } ?>
        <?php } ?>
    </section>
    <div id="my-modal" style="width: 100%; height: 100%; position: absolute; z-index: 900; background: rgba(0, 0, 0, .5); display: none">
        <div style="margin: 0 auto; width: 50%;">
            <section class="panel panel-default">
                <header class="panel-heading font-bold">
                    更改参数<button type="button" class="close pull-right">×</button>
                </header>
                <div class="panel-body">
                    <div class="form-group">
                        <label>K3</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>K2</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>K0</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>负荷类别</label>
                        <input type="text" class="form-control">
                    </div>
                    <button id="change-cfm" type="button" class="btn btn-sm btn-default">更改</button>
                </div>
            </section>
        </div>
    </div>
    <script src="<?php if(DIR_IN_ROOT) echo '/' . DIR_IN_ROOT ?>/public/js/app.v2.js"></script>
    <script src="<?php if(DIR_IN_ROOT) echo '/' . DIR_IN_ROOT ?>/public/js/ship.js"></script>
    <script>
        $('.close').on('click', function(){
            $('#my-modal').hide();
        });
        $('.fa-pencil').on('click', function(){
            var tr = $(this).parents('tr');
            //置标志位
            $('.editing').removeClass('editing');
            $(tr).addClass('editing');
            var tds = $(tr).find('td');
            var modal = $('#my-modal');
            //设置参数
            var k2 = $(tds[0]).text();
            var k3 = $(tds[1]).text();
            var k0 = $(tds[2]).text();
            var load_type = $(tds[3]).text();
            var inputs = $(modal).find('input');
            $(inputs[0]).val(k2);
            $(inputs[1]).val(k3);
            $(inputs[2]).val(k0);
            $(inputs[3]).val(load_type);
            $(modal).show();
        });
        $('#change-cfm').on('click', function(){
            var modal = $('#my-modal');
            var inputs = $(modal).find('input');
            var k2 = $(inputs[0]).val();
            var k3 = $(inputs[1]).val();
            var k0 = $(inputs[2]).val();
            var load_type = $(inputs[3]).val();
            var tr = $('.editing');
            var tds = $(tr).find('td');
            $(tds[0]).text(k2);
            $(tds[1]).text(k3);
            $(tds[2]).text(k0);
            $(tds[3]).text(load_type);
            //隐藏modal
            $(modal).hide();
        });
        $(".del-equip").on("click", function(){
            var id = $(this).parents('.ship-section').attr("id");
            $("[name='" + id +"']").remove(); //删侧边栏
            $("#" + id).remove(); //删section
            //首页显示
            $('#index-target').removeClass("none");
        });
        $(".update-equip").on("click", function(){
            var ship = $(this).parents('.ship-section');
            console.log(ship);
            var p1 = $(ship).find('[name="p1"]').val();
            var n = $(ship).find('[name="n"]').val();
            var number = $(ship).find('[name="number"]').val();
            var tds;
            var data = {};
            data.number = number;
            data.id = $(ship).find("[name='id']").val();
            //sail
            var sail_state = $(ship).find(".sail-state");
            tds = $(sail_state).find('td');
            data.k2_sail = $(tds[0]).text();
            data.k3_sail = $(tds[1]).text();
            data.k0_sail = $(tds[2]).text();
            data.sail_load_type = $(tds[3]).text();
            //inout
            var inout_state = $(ship).find(".inout-state");
            tds = $(inout_state).find('td');
            data.k2_inout = $(tds[0]).text();
            data.k3_inout = $(tds[1]).text();
            data.k0_inout = $(tds[2]).text();
            data.inout_load_type = $(tds[3]).text();
            //work_state
            var work_state = $(ship).find(".work-state");
            tds = $(work_state).find('td');
            data.k2_work = $(tds[0]).text();
            data.k3_work = $(tds[1]).text();
            data.k0_work = $(tds[2]).text();
            data.work_load_type = $(tds[3]).text();
            //anchor
            var anchor_state = $(ship).find(".anchor-state");
            tds = $(anchor_state).find('td');
            data.k2_anchor = $(tds[0]).text();
            data.k3_anchor = $(tds[1]).text();
            data.k0_anchor = $(tds[2]).text();
            data.anchor_load_type = $(tds[3]).text();
            console.log(data);
            $.ajax({
                type: "POST",
                url: "<?=site_url()?>/home/update",
                dataType: 'json',
                data: data,
                success: function(json){
                    console.log(json);
                },
                error: function (e) {
                    console.log(e);
                }
            });
        });
    </script>
</body>
</html>

