<?php
/**
 * Created by PhpStorm.
 * User: libingtao
 * Date: 15/12/22
 * Time: 下午8:52
 */
class Mod_equip extends CI_Model
{

    var $db;

    public function __construct()
    {
        parent::__construct();
    }

    public function init($db)
    {
        $this->db = $db;
    }

    public function add_equip($data){
        $this->db->insert('equip', $data);
    }

    public function update_equip($data){
        try {
            $this->db->where('id', $data['id']);
            $this->db->update('equip', $data);
        } catch (Exception $e){
            throw new Exception($e);
        }
    }

    public function insert_batch($batch_data){
        try {
            $this->db->insert_batch('equip', $batch_data);
        } catch(Exception $e){
            throw new Exception($e);
        }
    }

    public function get_all_equip(){
        return $this->db->get('equip')->result_array();
    }

}