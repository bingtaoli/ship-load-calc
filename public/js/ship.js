/**
 * Created by libingtao on 15/12/25.
 */
$('.belong-item').on('click', '.ship-item', function(){
    var name = $(this).attr('name');
    var target = $('#' + name);
    //相应船舶信息显示
    $(target).siblings('section').addClass('none');
    $(target).removeClass('none');
});
$('#index').on('click', function(){
    var target = $('#index-target');
    //首页显示
    $(target).siblings('section').addClass('none');
    $(target).removeClass('none');
});
$('#add-ship').on('click', function(){
    var target = $('#add-ship-target');
    $(target).siblings('section').addClass('none');
    $(target).removeClass('none');
});
$('#calculate').on('click', function(){
    var result = get_three_load();
    //console.log(result);
    //绘制表格
    var tbody = $('#calc-result').find('tbody');
    var trs = $(tbody).find('tr');
    var i, j;
    //4个状态
    for (i = 0; i < 4; i++){
        //3个负荷类别
        for (j = 0; j < 3; j++){
            //result横为负荷类别,纵为状态
            $($(trs[j]).find('td')[i+1]).text(result[i][j].toFixed(2));
        }
    }
    //I类考虑0.9
    for (i = 0; i < 4; i++){
        $($(trs[3]).find('td')[i+1]).text((0.9 * result[i][0]).toFixed(2));
    }
    //II类考虑0.6
    for (i = 0; i < 4; i++){
        $($(trs[4]).find('td')[i+1]).text((0.6 * result[i][1]).toFixed(2));
    }
    //I,II之和
    for (i = 0; i < 4; i++){
        $($(trs[5]).find('td')[i+1]).text((0.9 * result[i][0] + 0.6 * result[i][1]).toFixed(2));
    }
    var number = 0;  //所有发电机数
    var after_decrease_power;
    for (i = 0; i < 4; i++){
        //考虑网络损失5%后所需功率
        after_decrease_power = ((0.9 * result[i][0] + 0.6 * result[i][1]) / 0.95);
        $($(trs[6]).find('td')[i+1]).text(after_decrease_power.toFixed(2));
        var temp = parseInt(after_decrease_power / 90) + 1;
        number = number > temp ? number : temp;
    }
    number = number + 1;
    for (i = 0; i < 4; i++){
        //运行发电机台数
        after_decrease_power = ((0.9 * result[i][0] + 0.6 * result[i][1]) / 0.95);
        var temp = parseInt(after_decrease_power / 90) + 1;
        $($(trs[7]).find('td')[i+1]).text(temp + " X 90");
        //备用发电机数
        var backup = number - temp;
        $($(trs[8]).find('td')[i+1]).text(backup + " X 90");
        //发电机负荷率
        var load_rate = (after_decrease_power / (temp * 90)) * 100;
        $($(trs[9]).find('td')[i+1]).text(load_rate.toFixed(2));
    }
});
function get_three_load(){
    var ship_sections = $('.ship-section');
    var sail_first_load = 0, sail_second_load = 0, sail_third_load = 0;
    var inout_first_load = 0, inout_second_load = 0, inout_third_load = 0;
    var work_first_load = 0, work_second_load = 0, work_third_load = 0;
    var anchor_first_load = 0, anchor_second_load = 0, anchor_third_load = 0;
    for (var i = 0; i < ship_sections.length; i++){
        var ship = ship_sections[i];
        var p1 = $(ship).find('[name="p1"]').val();
        var n = $(ship).find('[name="n"]').val();
        var number = $(ship).find('[name="number"]').val();
        var P = p1 / n * 100 * number;
        var tds;
        var temp;
        //sail
        var sail_state = $(ship).find(".sail-state");
        tds = $(sail_state).find('td');
        var k2_sail = $(tds[0]).text();
        var k3_sail = $(tds[1]).text();
        var k0_sail = $(tds[2]).text();
        var sail_load_type = $(tds[3]).text();
        if (k2_sail != '-' && k3_sail != '-' && k0_sail != '-'){
            temp = P * k3_sail * k0_sail;
            temp = Math.floor(temp * 100) / 100;
            //console.log(temp);
            if (sail_load_type == 1){
                sail_first_load += temp;
            } else if (sail_load_type == 2){
                sail_second_load += temp;
            } else if (sail_load_type == 3){
                sail_third_load += temp;
            }
        }
        //inout
        var inout_state = $(ship).find(".inout-state");
        tds = $(inout_state).find('td');
        var k2_inout = $(tds[0]).text();
        var k3_inout = $(tds[1]).text();
        var k0_inout = $(tds[2]).text();
        var inout_load_type = $(tds[3]).text();
        if (k2_inout != '-' && k3_inout != '-' && k0_inout != '-'){
            temp = P * k3_inout * k0_inout;
            temp = Math.floor(temp * 100) / 100;
            if (inout_load_type == 1){
                inout_first_load += temp;
            } else if (inout_load_type == 2){
                inout_second_load += temp;
            } else if (inout_load_type == 3){
                inout_third_load += temp;
            }
        }
        //work_state
        var work_state = $(ship).find(".work-state");
        tds = $(work_state).find('td');
        var k2_work = $(tds[0]).text();
        var k3_work = $(tds[1]).text();
        var k0_work = $(tds[2]).text();
        var work_load_type = $(tds[3]).text();
        if (k2_work != '-' && k3_work != '-' && k0_work != '-'){
            temp = P * k3_work * k0_work;
            temp = Math.floor(temp * 100) / 100;
            if (work_load_type == 1){
                work_first_load += temp;
            } else if (work_load_type == 2){
                work_second_load += temp;
            } else if (work_load_type == 3){
                work_third_load += temp;
            }
        }
        //anchor
        var anchor_state = $(ship).find(".anchor-state");
        tds = $(anchor_state).find('td');
        var k2_anchor = $(tds[0]).text();
        var k3_anchor = $(tds[1]).text();
        var k0_anchor = $(tds[2]).text();
        var anchor_load_type = $(tds[3]).text();
        if (k2_anchor != '-' && k3_anchor != '-' && k0_anchor != '-'){
            temp = P * k3_anchor * k0_anchor;
            temp = Math.floor(temp * 100) / 100;
            if (anchor_load_type == 1){
                anchor_first_load += temp;
            } else if (anchor_load_type == 2){
                anchor_second_load += temp;
            } else if (anchor_load_type == 3){
                anchor_third_load += temp;
            }
        }
    }
    return [
        //横向分负荷,纵向分状态
        [sail_first_load, sail_second_load, sail_third_load],
        [inout_first_load, inout_second_load, inout_third_load],
        [work_first_load, work_second_load, work_third_load],
        [anchor_first_load, anchor_second_load, anchor_third_load]
    ];
}
function find_by_name(elem, name){
    return $(elem).find("[name='"+name+"']");
}
$('#add-ship-cfm').on('click', function() {
    //获取数据
    var source = $('#add-ship-target');
    var p1 = $(source).find("[name='p1']").val();
    var n = $(source).find("[name='n']").val();
    var k2_sail = $(source).find("[name='k2_sail']").val();
    var k3_sail = $(source).find("[name='k3_sail']").val();
    var k0_sail = $(source).find("[name='k0_sail']").val();
    var sail_load_type = $(source).find("[name='sail_load_type']").val();

    var k2_inout = $(source).find("[name='k2_inout']").val();
    var k3_inout = $(source).find("[name='k3_inout']").val();
    var k0_inout = $(source).find("[name='k0_inout']").val();
    var inout_load_type = $(source).find("[name='inout_load_type']").val();

    var k2_work = $(source).find("[name='k2_work']").val();
    var k3_work = $(source).find("[name='k3_work']").val();
    var k0_work = $(source).find("[name='k0_work']").val();
    var work_load_type = $(source).find("[name='work_load_type']").val();

    var k2_anchor = $(source).find("[name='k2_anchor']").val();
    var k3_anchor = $(source).find("[name='k3_anchor']").val();
    var k0_anchor = $(source).find("[name='k0_anchor']").val();
    var anchor_load_type = $(source).find("[name='anchor_load_type']").val();

    var belong = $(source).find("[name='belong']").val();
    var name = $(source).find("[name='equip-name']").val();
    //建立一个新的section
    var ship = $('.ship-section')[0];
    var ship_clone = $(ship).clone();
    //设置变量
    $(ship_clone).find("[name='k2_sail']").text(k2_sail);
    $(ship_clone).find("[name='k3_sail']").text(k3_sail);
    $(ship_clone).find("[name='k0_sail']").text(k0_sail);
    $(ship_clone).find("[name='sail_load_type']").text(sail_load_type);

    $(ship_clone).find("[name='k2_inout']").text(k2_inout);
    $(ship_clone).find("[name='k3_inout']").text(k3_inout);
    $(ship_clone).find("[name='k0_inout']").text(k0_inout);
    $(ship_clone).find("[name='inout_load_type']").text(inout_load_type);

    $(ship_clone).find("[name='k2_work']").text(k2_work);
    $(ship_clone).find("[name='k3_work']").text(k3_work);
    $(ship_clone).find("[name='k0_work']").text(k0_work);
    $(ship_clone).find("[name='work_load_type']").text(work_load_type);

    $(ship_clone).find("[name='k2_anchor']").text(k2_anchor);
    $(ship_clone).find("[name='k3_anchor']").text(k3_anchor);
    $(ship_clone).find("[name='k0_anchor']").text(k0_anchor);
    $(ship_clone).find("[name='anchor_load_type']").text(anchor_load_type);

    $(ship_clone).find("[name='p1']").val(p1);
    $(ship_clone).find("[name='n']").val(n);
    //添加到侧边栏
    belong = belong -1;
    var lis = $('.belong-item');
    var equip_ul = $(lis[belong]).find('ul');
    var equip_items = $(lis[belong]).find('.ship-item');
    var last_equip = $(equip_items[equip_items.length-1]);
    var last_name = $(last_equip).attr("name");
    var first_section = last_name.split("_")[0];
    var second_section = parseFloat(last_name.split("_")[1]) + 1;
    var li_clone = $(last_equip).clone();
    $(li_clone).attr("name", first_section + "_" + second_section);
    $(li_clone).find("span").text(name);
    $(equip_ul).append(li_clone);
    //增加section
    var ship_sections = $(".ship-section");
    var sec_clone = $(ship_sections[0]).clone();
    $(sec_clone).attr("id", first_section + "_" + second_section);
    $(sec_clone).find("[name='p1']").val(p1);
    $(sec_clone).find("[name='n']").val(n);
    find_by_name(sec_clone, "k2_sail").text(k2_sail ? k2_sail : "-");
    find_by_name(sec_clone, "k3_sail").text(k3_sail ? k3_sail : "-");
    find_by_name(sec_clone, "k0_sail").text(k0_sail ? k0_sail : "-");
    find_by_name(sec_clone, "sail_load_type").text(sail_load_type ? sail_load_type : "-");

    find_by_name(sec_clone, "k2_inout").text(k2_inout ? k2_inout : "-");
    find_by_name(sec_clone, "k3_inout").text(k3_inout ? k3_inout : "-");
    find_by_name(sec_clone, "k0_inout").text(k0_inout ? k0_inout : "-");
    find_by_name(sec_clone, "inout_load_type").text(inout_load_type ? inout_load_type : "-");

    find_by_name(sec_clone, "k2_work").text(k2_work ? k2_work : "-");
    find_by_name(sec_clone, "k3_work").text(k3_work ? k3_work : "-");
    find_by_name(sec_clone, "k0_work").text(k0_work ? k0_work : "-");
    find_by_name(sec_clone, "work_load_type").text(work_load_type ? work_load_type : "-");

    find_by_name(sec_clone, "k2_anchor").text(k2_anchor ? k2_anchor : "-");
    find_by_name(sec_clone, "k3_anchor").text(k3_anchor ? k3_anchor : "-");
    find_by_name(sec_clone, "k0_anchor").text(k0_anchor ? k0_anchor : "-");
    find_by_name(sec_clone, "anchor_load_type").text(anchor_load_type ? anchor_load_type : "-");
    $(ship_sections[ship_sections.length - 1]).after(sec_clone);
});